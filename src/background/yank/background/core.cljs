(ns yank.background.core
  (:require-macros [shared.logging :as d])
  (:require [goog.object :as gobj]
            [shared.options :refer [fetch-options defaults on-storage-change]]
            [yank.background.format :as format]))

;; for extern inference. Better warnings
(set! *warn-on-infer* true)

(def ^js/browser tabs (gobj/get js/browser "tabs"))
(def ^js/browser runtime (gobj/get js/browser "runtime"))
(def ^js/browser context-menus (gobj/get js/browser "contextMenus"))

(def options (atom defaults))

(defn create-context-menu
  []
  (.create context-menus (clj->js {:id "yank-link"
                                   :title "Yank link to clipboard"
                                   :contexts ["link"]})))
(defn execute-script
  "Execute a script using js/browser.tabs
  'obj' param is a javascript object conforming to this:
  https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/tabs/executeScript
  optionally takes a tab-id to select which tab to execute script inside
  Returns a js/Promise"
  ([obj]
   (.executeScript tabs (clj->js obj)))
  ([obj tab-id]
   (.executeScript tabs tab-id (clj->js obj))))

(defn load-clipboard-helper
  "load js function defined in clipboard-helper.js"
  [tab-id]
  (-> ^js/Promise (execute-script {:code "typeof copyToClipboard === 'function';"})
      (.then (fn [result]
               (when (or  (not result) (false? (first result)))
                 (execute-script {:file "clipboard-helper.js"} tab-id))))
      (.catch (fn [error]
                (d/error "Failed to load clipboard helper: " error)))))

(defn copy-to-clipboard
  "Copy text to clipboard by injecting the formatted input (text)
  as an argment to the loaded 'copyToClipboard"
  [tab-id text]
  (let [code (str "copyToClipboard(" (.stringify js/JSON text) ");")]
    (-> ^js/Promise (load-clipboard-helper tab-id)
        (.then (fn []
                 (execute-script {:code code} tab-id )))
        (.catch (fn [error]
                  (d/error "Failed to copy text: " error))))))

(defn callback-with-tabs [{:keys [action tab-id]} tabs]
  #_(doseq [tabs (array-seq tabs)]
      (d/log (.-url tab)))
  (let [tabs (array-seq tabs)]
    (->> tabs
         (reduce (fn [ans tab]
                   #_(do (d/log (gobj/get tab "url")))
                   (str ans
                        (format/as {:action action
                                    :url (gobj/get tab "url")
                                    :title (gobj/get tab "title")})
                        "\n"))
                 "")
         (copy-to-clipboard tab-id))))

(defn get-tabs-url [{:keys [action tab-id] :as stuff} obj]
  "obj := tabs.query(obj)"
  #_(do (d/log "Action and tab-id:---- " action " --- " tab-id " --- "))
  (-> ^js/Promise (.query tabs (clj->js obj))
      (.then #(callback-with-tabs stuff %))
      (.catch (fn [error]
                (d/error "Failed to get tabs.")))))

#_(defn handle-message
  "Handle incoming runtime message, extract info and call copy-as"
  [request sender send-response]
  (when-some [action (gobj/get request "action")]
    (let [tab (gobj/get sender "tab")
          url (gobj/get tab "url")
          tab-id (gobj/get tab "id")
          title (gobj/get tab "title")
         text (format/as {:action action
                          :url url
                          :title title})]
      (copy-to-clipboard tab-id text))))

(defn handle-message
  "Handle incoming runtime message, extract info and call copy-as"
  [request sender send-response]
  (when-some [action (gobj/get request "action")]
    (when-some [tab-option (gobj/get request "tab")]
      (let [stuff {:action action :tab-id (-> sender
                                              (gobj/get "tab")
                                              (gobj/get "id"))}]
        (case tab-option
          "tab"
          (let [tab (gobj/get sender "tab")
                url (gobj/get tab "url")
                tab-id (gobj/get tab "id")
                title (gobj/get tab "title")
                text (format/as {:action action
                                 :url url
                                 :title title})]
            (copy-to-clipboard tab-id text))
          "tabs"
          (get-tabs-url stuff {:currentWindow true})
          ;;(d/log "For tabs NOT IMPLEMENTED")
          "tabs-all"
          (get-tabs-url stuff {})
          ;;(d/log "For tabs-all NOT IMPLEMENTED")
          ;;default:
          (d/error  "Error: tab-option, " tab-option ", not implemented."))))))

(defn handle-click
  []
  (.openOptionsPage runtime))

(defn handle-context
  [info tab]
  (let [url (gobj/get info "linkUrl")
        tab-id (gobj/get tab "id")
        text (gobj/get info "linkText")
        action (:action @options)
        text (format/as {:action action
                         :url url
                         :title text})]
    (copy-to-clipboard tab-id text)))

(defn fig-reload
  []
  (.reload runtime))

(defn init!
  []
  (create-context-menu)
  (fetch-options options)
  (.addListener ^js/browser
                (gobj/getValueByKeys js/browser "storage" "onChanged")
                #(on-storage-change options %))
  (.addListener ^js/browser
                (gobj/getValueByKeys js/browser
                                     "browserAction" "onClicked")
                handle-click)
  (.addListener ^js/browser
                (gobj/getValueByKeys js/browser
                                     "contextMenus" "onClicked")
                handle-context)
  (.addListener ^js/browser (gobj/get runtime "onMessage") handle-message))
