
VERSION_NUMBER=1.0.7

# Firefox requires to submit source-code when signing addon
tar-src-code:
	mkdir -p releases/source_code/
	tar -czvf releases/source_code/n-yank-source-code-$(VERSION_NUMBER).tar.gz src/ README.org README.md test/ script/ resources/ dev/

build-for-signing: tar-src-code #Note: need to manually update version number.
	lein release
