#+TITLE: n-yank (a roosta/yank fork)

This is a fork of roosta/yank firefox web-extension

* Additions
- It is possible to add to clipboard all tabs...
  - in current window
  - or all tabs from all windows

* Dev workflow

For more see roosta/yank's original [[file:/home/nm/Documents/ProgrammingWorkspace/n-yank/README.md][README.md]].

1. For building html, css and manifest: ~lein build~
2. ~lein repl~ and then (in repl) ~>(start)~.
3. In browser go to ~about:debugging~ and then ~Load Temporary Add-on~ and select
   ~<path>/resources/dev/manifest.json~.
4. For seeing logging:
   1. Enter ~about:debugging~ in the URL bar.
   2. In the left-hand menu, click ~This Firefox~.
   3. Click ~Inspect~ next to your extension.
5. For building a release:
   1. ~lein release~
   2. To test as temporary addon, load ~./releases/n-yank-<VERSION>.xpi~ in
      ~about:debugging~.
   3. For using web-extension permanently, you need to sign it.
      1. Firefox requires to submit code too. For that: ~make tar-src-code~.
      2. Go to your personal page [[https://addons.mozilla.org/en-US/developers/addons][Manage my submissions :: Developer Hub :: Add-ons for Firefox]]
      3. Click ~Submit a new Addon~
      4. Select ~xpi~ file in release folder and submit code.
      5. To install addon on browser: Wait for email to confirm, open link,
         under "All versions" click on ~Version <version_number>~ and then click
         on ~n_yank-<version_number>-fx.xpi~.

* Resources: 
- Original: [[https://github.com/roosta/yank][roosta/yank: Firefox extension that yanks URL]]
- Dev
  - [[https://extensionworkshop.com/documentation/develop/debugging/][Debugging | Firefox Extension Workshop]]
  - MDN Web docs
    - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Anatomy_of_a_WebExtension][Anatomy of an extension - Mozilla | MDN]]
      - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Content_scripts][Content scripts - Mozilla | MDN]]
      - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API][JavaScript APIs - Mozilla | MDN]]
      - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Browser_support_for_JavaScript_APIs][Browser support for JavaScript APIs - Mozilla | MDN]]
    - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/tabs][tabs - Mozilla | MDN]]
    - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/tabs/query][tabs.query() - Mozilla | MDN]]
    - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/sendMessage][runtime.sendMessage() - Mozilla | MDN]]
    - [[https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/browser_specific_settings][browser_specific_settings - Mozilla | MDN]]
  - Tests
    - [[https://github.com/igrishaev/etaoin][igrishaev/etaoin: Pure Clojure Webdriver protocol implementation]] Have not tested it yet.
